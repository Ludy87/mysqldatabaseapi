package org.astra_g.mysqldatabaseapi;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.astra_g.mysqldatabaseapi.adapter.DeviceAdapter;
import org.astra_g.mysqldatabaseapi.api.ApiService;
import org.astra_g.mysqldatabaseapi.helper.ItemClickSupport;
import org.astra_g.mysqldatabaseapi.interfaces.ItemRemoveCallback;
import org.astra_g.mysqldatabaseapi.model.Delete;
import org.astra_g.mysqldatabaseapi.model.Devices;
import org.astra_g.mysqldatabaseapi.model.Insert;
import org.astra_g.mysqldatabaseapi.model.Update;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class MainActivity extends AppCompatActivity {

    private RecyclerView mRecyclerView;
    private Retrofit restApi;
    private ApiService apiService;
    private Call<Devices> mDevicesCall;
    private DeviceAdapter adapter;
    private ArrayList<Devices.Details> details;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        restApi = new Retrofit.Builder()
                .baseUrl(ApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        apiService = restApi.create(ApiService.class);
        mDevicesCall = apiService.getDevices();
        mDevicesCall.enqueue(new Callback<Devices>() {
            @Override
            public void onResponse(Call<Devices> call, Response<Devices> response) {
                details = response.body().getDetails();
                adapter = new DeviceAdapter(details);
                mRecyclerView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Devices> call, Throwable t) {
                Log.e("TAG", t.getLocalizedMessage());
            }
        });
        ItemClickSupport.addTo(mRecyclerView).setOnItemLongClickListener(new ItemClickSupport.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClicked(RecyclerView recyclerView, int position, View v) {
                Toast.makeText(getBaseContext(), details.get(position).getId(), Toast.LENGTH_LONG).show();
                showDialogUpdate(MainActivity.this, details.get(position).getId());
                return true;
            }
        });

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(mRecyclerView);
    }

    private ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT) {

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            final String _id = details.get(viewHolder.getAdapterPosition()).getId();
            adapter.onItemRemove(viewHolder, mRecyclerView, new ItemRemoveCallback() {

                @Override
                public void get() {
                    Call<Delete> deleteCall = apiService.deleteDevice(_id);
                    deleteCall.enqueue(new Callback<Delete>() {
                        @Override
                        public void onResponse(Call<Delete> call, Response<Delete> response) {
                            if (response.body().getSuccess() != -1) {
                                Toast.makeText(getApplicationContext(), "ok", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<Delete> call, Throwable t) {
                            Log.e("TAG", t.getLocalizedMessage());
                        }
                    });
                }
            });
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                showDialogInsert(MainActivity.this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showDialogUpdate(Context context, final String _id) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompts, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setTitle("Device update");
        alertDialogBuilder.setView(promptsView);

        final EditText userInputManufacturer = (EditText) promptsView
                .findViewById(R.id.editTextDialogManufacturer);
        final EditText userInputType = (EditText) promptsView
                .findViewById(R.id.editTextDialogType);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String manufacturer = userInputManufacturer.getText().toString();
                                String type = userInputType.getText().toString();
                                Call<Update> insertCall = apiService.updateDevice(_id, manufacturer, type);
                                insertCall.enqueue(new Callback<Update>() {
                                    @Override
                                    public void onResponse(Call<Update> call, Response<Update> response) {
                                        switch (response.body().getSuccess()) {
                                            case -1:
                                                Toast.makeText(getApplicationContext(), "etwas ist schief gelaufen.", Toast.LENGTH_LONG).show();
                                                break;
                                            case 1:
                                                Toast.makeText(getApplicationContext(), "Alle OK.", Toast.LENGTH_LONG).show();
                                                break;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Update> call, Throwable t) {
                                        Log.e("Fail", t.getLocalizedMessage());
                                    }
                                });
                            }
                        })
                .setNegativeButton(getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    private void showDialogInsert(Context context) {
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.prompts, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);
        alertDialogBuilder.setTitle("Device added");
        alertDialogBuilder.setView(promptsView);

        final EditText userInputManufacturer = (EditText) promptsView
                .findViewById(R.id.editTextDialogManufacturer);
        final EditText userInputType = (EditText) promptsView
                .findViewById(R.id.editTextDialogType);

        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton(getString(android.R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                String manufacturer = userInputManufacturer.getText().toString();
                                String type = userInputType.getText().toString();
                                Call<Insert> insertCall = apiService.insertDevice(manufacturer, type);
                                insertCall.enqueue(new Callback<Insert>() {
                                    @Override
                                    public void onResponse(Call<Insert> call, Response<Insert> response) {
                                        switch (response.body().getSuccess()) {
                                            case -1:
                                                Toast.makeText(getApplicationContext(), "etwas ist schief gelaufen.", Toast.LENGTH_LONG).show();
                                                break;
                                            case 1:
                                                Toast.makeText(getApplicationContext(), "Alle OK.", Toast.LENGTH_LONG).show();
                                                break;
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<Insert> call, Throwable t) {
                                        Log.e("Fail", t.getLocalizedMessage());
                                    }
                                });
                            }
                        })
                .setNegativeButton(getString(android.R.string.cancel),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}
