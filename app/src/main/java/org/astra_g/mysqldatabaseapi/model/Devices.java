package org.astra_g.mysqldatabaseapi.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Ludy on 19.10.2016.
 */

public class Devices {

    @SerializedName("devices")
    private ArrayList<Details> details;

    public ArrayList<Details> getDetails() {
        return details;
    }

    public class Details {
        private String id;
        private String manufacturer;
        private String type;

        public String getId() {
            return id;
        }

        public String getManufacturer() {
            return manufacturer;
        }

        public String getType() {
            return type;
        }
    }
}