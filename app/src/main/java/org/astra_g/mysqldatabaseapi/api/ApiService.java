package org.astra_g.mysqldatabaseapi.api;

import org.astra_g.mysqldatabaseapi.model.Delete;
import org.astra_g.mysqldatabaseapi.model.Devices;
import org.astra_g.mysqldatabaseapi.model.Insert;
import org.astra_g.mysqldatabaseapi.model.Update;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by Ludy on 19.10.2016.
 */

public interface ApiService {

    String BASE_URL = "http://192.168.188.20:8080";

    @GET("/api/devices.php")
    Call<Devices> getDevices();

    @FormUrlEncoded
    @POST("/api/insert.php")
    Call<Insert> insertDevice(
            @Field("manufacturer")
            String manufacturer,
            @Field("type")
            String type
    );

    @FormUrlEncoded
    @POST("/api/update.php")
    Call<Update> updateDevice(
            @Field("id")
            String id,
            @Field("manufacturer")
            String manufacturer,
            @Field("type")
            String type
    );

    @FormUrlEncoded
    @POST("/api/delete.php")
    Call<Delete> deleteDevice(
            @Field("id")
            String id
    );
}
