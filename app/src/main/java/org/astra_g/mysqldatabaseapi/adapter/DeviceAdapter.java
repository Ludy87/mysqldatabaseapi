package org.astra_g.mysqldatabaseapi.adapter;

import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.astra_g.mysqldatabaseapi.R;
import org.astra_g.mysqldatabaseapi.interfaces.ItemRemoveCallback;
import org.astra_g.mysqldatabaseapi.model.Devices;

import java.util.ArrayList;

/**
 * Created by Ludy on 20.10.2016.
 */

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.ViewHolder> {

    private ArrayList<Devices.Details> mDetails;
    private ItemRemoveCallback itemRemoveCallback;

    public DeviceAdapter(ArrayList<Devices.Details> details) {
        mDetails = details;
    }

    @Override
    public DeviceAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_view_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(DeviceAdapter.ViewHolder holder, int position) {
        Devices.Details details = mDetails.get(position);
        holder.textViewId.setText(details.getId());
        holder.textViewManufacturer.setText(details.getManufacturer());
        holder.textViewType.setText(details.getType());
    }

    @Override
    public int getItemCount() {
        return mDetails.size();
    }

    public void onItemRemove(RecyclerView.ViewHolder viewHolder, final RecyclerView mRecyclerView, final ItemRemoveCallback itemRemoveCallback) {
        this.itemRemoveCallback = itemRemoveCallback;
        final int adapterPosition = viewHolder.getAdapterPosition();
        final Devices.Details details = mDetails.get(adapterPosition);
        Snackbar snackbar = Snackbar.make(mRecyclerView, "removed", Snackbar.LENGTH_LONG)
                .setAction("undo", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mDetails.add(adapterPosition, details);
                        notifyItemInserted(adapterPosition);
                        mRecyclerView.scrollToPosition(adapterPosition);
                    }
                })
                .addCallback(new Snackbar.Callback() {
                    @Override
                    public void onDismissed(Snackbar transientBottomBar, int event) {
                        super.onDismissed(transientBottomBar, event);
                        if (event != DISMISS_EVENT_ACTION) {
                            itemRemoveCallback.get();
                        }
                    }
                });
        snackbar.show();
        mDetails.remove(adapterPosition);
        notifyItemRemoved(adapterPosition);
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView textViewId;
        private TextView textViewManufacturer;
        private TextView textViewType;

        ViewHolder(View itemView) {
            super(itemView);
            this.textViewId = (TextView) itemView.findViewById(R.id.textViewId);
            this.textViewManufacturer = (TextView) itemView.findViewById(R.id.textViewManufacturer);
            this.textViewType = (TextView) itemView.findViewById(R.id.textViewType);
        }
    }
}
